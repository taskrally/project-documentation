# Сервис рассылки уведомлений

Сервис рассылки уведомлений служит для формирования уведомлений на основе каких-либо действий в системе и отсылка их заинтересованным лицам по разным каналам.


## Основные элементы сервиса

Работа сервиса построена на:

* **Redis** - используется сторонними библиотеками для своих нужд
* **Firestore DB** - база данных от гугла
* **Master** - элемент сервиса, отвечающий за обработку действий в системе, формирования и контроля работ по созданию уведомлений и рассылки
* **Worker** - элемент сервиса, отвечающий за исполнения работ по генерации уведомлений и рассылки

Также используются библиотеки:

* **Kue** - менеджер очереди работ. Предоставляет АПИ по созданию очередей, распределению работ между воркерами и прочее, для работы необходим **Redis**. Алтернативой может служить **Bull**, но пока хватает ее.
* **cron-cluster** - библиотека понадобится для **Master**. Под капотом держится на **redis-leader** библиотеке. Берет на себя определение лидера, который будет выполнять работу в данный момент среди нескольких **Master**.

![Infrastructure](./notification-service/infr.svg)

## Модели данных

### M1. Статусы выполениния работы

<table>
  <tr>
    <th>Название</th>
    <th>Значение</th>
    <th>Описание</th>
  </tr>
  <tr>
    <td>processing</td>
    <td>0</td>
    <td>Выполняется</td>
  </tr>
  <tr>
    <td>done</td>
    <td>2</td>
    <td>Завершена</td>
  </tr>
</table>

### M2. Действие

<table>
  <tr>
    <th>Поле</th>
    <th>Тип</th>
    <th>Описание</th>
  </tr>
  <tr>
    <td>id</td>
    <td>string</td>
    <td>ID записи</td>
  </tr>
  <tr>
    <td>userId</td>
    <td>string</td>
    <td>ID пользователя. Инициатор действия</td>
  </tr>
  <tr>
    <td>objectId</td>
    <td>string</td>
    <td>ID объекта. Объект затрагиваемый в действии</td>
  </tr>
  <tr>
    <td>structureName</td>
    <td>string.<br><br>Может быть: task</td>
    <td>Название объекта, затрагиваемого в действии</td>
  </tr>
  <tr>
    <td>organizationId</td>
    <td>string</td>
    <td>ID организации</td>
  </tr>
  <tr>
    <td>createdAt</td>
    <td>integer</td>
    <td>Время создания записи</td>
  </tr>
  <tr>
    <td>name</td>
    <td>string<br><br>Может быть: task_create, task_update_status, task_change_type<br></td>
    <td>Название действия</td>
  </tr>
  <tr>
    <td>previousValue</td>
    <td>string, JSON, null</td>
    <td>Значение до действия</td>
  </tr>
  <tr>
    <td>currentValue</td>
    <td>string, JSON, null</td>
    <td>Значение после действия</td>
  </tr>
</table>

### M3. Уведомление. Тип "Тик"

<table>
  <tr>
    <th>Поле</th>
    <th>Тип</th>
    <th>Описание</th>
  </tr>
  <tr>
    <td>id</td>
    <td>string</td>
    <td>ID записи</td>
  </tr>
  <tr>
    <td>type</td>
    <td>string<br><br>Может быть: task</td>
    <td>Тип уведомления</td>
  </tr>
  <tr>
    <td>subType</td>
    <td>string<br><br><br>Если type равен task, то может быть: task_create, task_update_status, task_change_type</td>
    <td>Подтип уведомления</td>
  </tr>
  <tr>
    <td>actionId</td>
    <td>string</td>
    <td>ID действия</td>
  </tr>
  <tr>
    <td>createdAt</td>
    <td>integer</td>
    <td>Время создания записи</td>
  </tr>
  <tr>
    <td>actionCreatedAt</td>
    <td>integer</td>
    <td>Время создания действия</td>
  </tr>
  <tr>
    <td>content</td>
    <td>string, JSON</td>
    <td>Текст уведомления или данные для генерации уведомления</td>
  </tr>
  <tr>
    <td>plannedDeliveryDate</td>
    <td>integer</td>
    <td>Планируемая дата доставки</td>
  </tr>
  <tr>
    <td>receiversIds</td>
    <td>string[]</td>
    <td>Список ID получателей уведомления </td>
  </tr>
</table>

### M4. Статусы доставки уведомления получателям. Тип "Тик"

<table>
  <tr>
    <th>Поле</th>
    <th>Тип</th>
    <th>Описание</th>
  </tr>
  <tr>
    <td>id</td>
    <td>string</td>
    <td>ID записи</td>
  </tr>
  <tr>
    <td>notificationId</td>
    <td>string</td>
    <td>ID уведомления</td>
  </tr>
  <tr>
    <td>createdAt</td>
    <td>integer</td>
    <td>Время создания записи</td>
  </tr>
  <tr>
    <td>receiverId</td>
    <td>string</td>
    <td>ID получателя</td>
  </tr>
  <tr>
    <td>plannedDeliveryDate</td>
    <td>integer</td>
    <td>Планируемая дата доставки уведомления</td>
  </tr>
  <tr>
    <td>status</td>
    <td>integer<br><br>Может быть: <br>0 - Отправляется<br>2 - Отправлено<br>3 - Ошибка отправки</td>
    <td>Статус доставки</td>
  </tr>
  <tr>
    <td>errorCode</td>
    <td>string</td>
    <td>Код ошибки отправки</td>
  </tr>
</table>

### M5. Статус уведомления. Тип "Тик"

<table>
  <tr>
    <th>Поле</th>
    <th>Тип</th>
    <th>Описание</th>
  </tr>
  <tr>
    <td>id</td>
    <td>string</td>
    <td>ID записи</td>
  </tr>
  <tr>
    <td>organizationId</td>
    <td>string</td>
    <td>ID организации</td>
  </tr>
  <tr>
    <td>startAt</td>
    <td>integer</td>
    <td>Дата от. Включает действия от данной даты</td>
  </tr>
  <tr>
    <td>endAt</td>
    <td>integer</td>
    <td>Дата по. Включает действия по данную дату</td>
  </tr>
  <tr>
    <td>actionsIds</td>
    <td>string[]</td>
    <td>Список ID действий</td>
  </tr>
  <tr>
    <td>notificationsIds</td>
    <td>string[]</td>
    <td>Список ID уведомлений, сгенерированных для действий</td>
  </tr>
  <tr>
    <td>isEmpty</td>
    <td>boolean</td>
    <td>Пустой ли тик(не включает действий)</td>
  </tr>
  <tr>
    <td>isReadyToSend</td>
    <td>boolean</td>
    <td>Готов ли тик для рассылки</td>
  </tr>
  <tr>
    <td>isCompleted</td>
    <td>boolean</td>
    <td>Завершена ли рассылка</td>
  </tr>
  <tr>
    <td>completedAt</td>
    <td>integer</td>
    <td>Дата завершения рассылки</td>
  </tr>
</table>

### M6. Статус работы по генерации уведомления. Тип "Тик"

<table>
  <tr>
    <th>Поле</th>
    <th>Тип</th>
    <th>Описание</th>
  </tr>
  <tr>
    <td>id</td>
    <td>string</td>
    <td>ID записи</td>
  </tr>
  <tr>
    <td>actionId</td>
    <td>string</td>
    <td>ID действия</td>
  </tr>
  <tr>
    <td>actionCreatedAt</td>
    <td>integer</td>
    <td>Время создания действия</td>
  </tr>
  <tr>
    <td>status</td>
    <td>integer<br><br>См. модель M1</td>
    <td>Статус выполения</td>
  </tr>
  <tr>
    <td>jobId</td>
    <td>integer</td>
    <td>ID работы</td>
  </tr>
  <tr>
    <td>completedAt</td>
    <td>integer</td>
    <td>Время завершения работы</td>
  </tr>
  <tr>
    <td>tickId</td>
    <td>string</td>
    <td>ID статуса уведомления типа "Тик"</td>
  </tr>
  <tr>
    <td>createdAt</td>
    <td>integer</td>
    <td>Время создания записи</td>
  </tr>
  <tr>
    <td>notificationId</td>
    <td>string</td>
    <td>ID сгенерированного уведомления</td>
  </tr>
</table>

### M7. Статус работы по отправке уведомления. Тип "Тик"


<table>
  <tr>
    <th>Поле</th>
    <th>Тип</th>
    <th>Описание</th>
  </tr>
  <tr>
    <td>id</td>
    <td>string</td>
    <td>ID записи</td>
  </tr>
  <tr>
    <td>status</td>
    <td>integer<br><br>См. модел M1</td>
    <td>Статус выполения</td>
  </tr>
  <tr>
    <td>jobId</td>
    <td>integer</td>
    <td>ID работы</td>
  </tr>
  <tr>
    <td>completedAt</td>
    <td>integer</td>
    <td>Время завершения работы</td>
  </tr>
  <tr>
    <td>tickId</td>
    <td>string</td>
    <td>ID статуса уведомления типа "Тик"</td>
  </tr>
  <tr>
    <td>createdAt</td>
    <td>integer</td>
    <td>Время создания записи</td>
  </tr>
  <tr>
    <td>plannedDeliveryDate</td>
    <td>integer</td>
    <td>Планируемая дата доставки</td>
  </tr>
</table>

### M8. Статус обработки действия

<table>
  <tr>
    <th>Поле</th>
    <th>Тип</th>
    <th>Описание</th>
  </tr>
  <tr>
    <td>id</td>
    <td>string</td>
    <td>ID записи</td>
  </tr>
  <tr>
    <td>actionId</td>
    <td>string</td>
    <td>ID действия</td>
  </tr>
  <tr>
    <td>actionCreatedAt</td>
    <td>integer</td>
    <td>Дата создани действия</td>
  </tr>
  <tr>
    <td>createdAt</td>
    <td>integer</td>
    <td>Время создания записи</td>
  </tr>
</table>

### M9. Интервал уведомления

<table>
  <tr>
    <th>Поле</th>
    <th>Тип</th>
    <th>Описание</th>
  </tr>
  <tr>
    <td>id</td>
    <td>string</td>
    <td>ID записи</td>
  </tr>
  <tr>
    <td>value</td>
    <td>string<br><br>Может быть:<br>1m - каждую минуту<br>5m - каждые 5 минут<br>10m - каждые 10 минут</td>
    <td>Интервал уведомления</td>
  </tr>
</table>

### M10. Оранизация

<table>
  <tr>
    <th>Поле</th>
    <th>Тип</th>
    <th>Описание</th>
  </tr>
  <tr>
    <td>id</td>
    <td>string</td>
    <td>ID записи</td>
  </tr>
  <tr>
    <td>notificationInterval</td>
    <td>structure<br><br>См. модель М9</td>
    <td>Интервал уведомления</td>
  </tr>
  <tr>
    <td>createdAt</td>
    <td>integer</td>
    <td>Время создания записи</td>
  </tr>
</table>

### M11. Задача

<table>
  <tr>
    <th>Поле</th>
    <th>Тип</th>
    <th>Описание</th>
  </tr>
  <tr>
    <td>sysId</td>
    <td>string</td>
    <td>ID записи</td>
  </tr>
  <tr>
    <td>author</td>
    <td>string</td>
    <td>ID автора задачи</td>
  </tr>
</table>

### M12. Пользователь

<table>
  <tr>
    <th>Поле</th>
    <th>Тип</th>
    <th>Описание</th>
  </tr>
  <tr>
    <td>id</td>
    <td>string</td>
    <td>ID записи</td>
  </tr>
  <tr>
    <td>email</td>
    <td>string</td>
    <td>Email</td>
  </tr>
  <tr>
      <td>telegramChatId</td>
      <td>string</td>
      <td>ID телеграм чата</td>
    </tr>
</table>

### M13. Зашифрованные данные пользователя 

<table>
  <tr>
    <th>Поле</th>
    <th>Тип</th>
    <th>Описание</th>
  </tr>
  <tr>
    <td>id</td>
    <td>string</td>
    <td>ID записи</td>
  </tr>
  <tr>
    <td>accountId</td>
    <td>string</td>
    <td>ID пользователя</td>
  </tr>
  <tr>
    <td>data</td>
    <td>string</td>
    <td>Зашифрованные данные</td>
  </tr>
  <tr>
      <td>iv</td>
      <td>string</td>
      <td>Вектор</td>
    </tr>
</table>


### M14. Уведомление. Тип "Коллизия"

<table>
  <tr>
    <th>Поле</th>
    <th>Тип</th>
    <th>Описание</th>
  </tr>
  <tr>
    <td>id</td>
    <td>string</td>
    <td>ID записи</td>
  </tr>
  <tr>
    <td>actionsIds</td>
    <td>string</td>
    <td>Список ID действий</td>
  </tr>
  <tr>
    <td>createdAt</td>
    <td>integer</td>
    <td>Время создания записи</td>
  </tr>
  <tr>
    <td>content</td>
    <td>string, JSON</td>
    <td>Текст уведомления или данные для генерации уведомления</td>
  </tr>
  <tr>
    <td>receiversIds</td>
    <td>string[]</td>
    <td>Список ID получателей уведомления</td>
  </tr>
</table>

### M15. Статусы доставки уведомления получателям. Тип "Коллизия"

<table>
  <tr>
    <th>Поле</th>
    <th>Тип</th>
    <th>Описание</th>
  </tr>
  <tr>
    <td>id</td>
    <td>string</td>
    <td>ID записи</td>
  </tr>
  <tr>
    <td>notificationId</td>
    <td>string</td>
    <td>ID уведомления</td>
  </tr>
  <tr>
    <td>createdAt</td>
    <td>integer</td>
    <td>Время создания записи</td>
  </tr>
  <tr>
    <td>receiverId</td>
    <td>string</td>
    <td>ID получателя</td>
  </tr>
  <tr>
    <td>status</td>
    <td>integer<br><br>Может быть: <br>0 - Отправляется<br>2 - Отправлено<br>3 - Ошибка отправки</td>
    <td>Статус доставки</td>
  </tr>
  <tr>
    <td>errorCode</td>
    <td>string</td>
    <td>Код ошибки отправки</td>
  </tr>
</table>

### M16. Статус уведомления. Тип "Коллизия"

<table>
  <tr>
    <th>Поле</th>
    <th>Тип</th>
    <th>Описание</th>
  </tr>
  <tr>
    <td>id</td>
    <td>string</td>
    <td>ID записи</td>
  </tr>
  <tr>
    <td>organizationId</td>
    <td>string</td>
    <td>ID организации</td>
  </tr>
  <tr>
    <td>actionsIds</td>
    <td>string[]</td>
    <td>Список ID действий</td>
  </tr>
  <tr>
    <td>notificationId</td>
    <td>string</td>
    <td>ID уведомления</td>
  </tr>
  <tr>
    <td>isEmpty</td>
    <td>boolean</td>
    <td>Пустой ли тик(не включает действий)</td>
  </tr>
  <tr>
    <td>isReadyToSend</td>
    <td>boolean</td>
    <td>Готов ли тик для рассылки</td>
  </tr>
  <tr>
    <td>isCompleted</td>
    <td>boolean</td>
    <td>Завершена ли рассылка</td>
  </tr>
  <tr>
    <td>completedAt</td>
    <td>integer</td>
    <td>Дата завершения рассылки</td>
  </tr>
</table>

### M17. Статус работы по генерации уведомления. Тип "Коллизия"

<table>
  <tr>
    <th>Поле</th>
    <th>Тип</th>
    <th>Описание</th>
  </tr>
  <tr>
    <td>id</td>
    <td>string</td>
    <td>ID записи</td>
  </tr>
  <tr>
    <td>actionsIds</td>
    <td>string[]</td>
    <td>Список ID действий</td>
  </tr>
  <tr>
    <td>status</td>
    <td>integer<br><br>См. модель M1</td>
    <td>Статус выполения</td>
  </tr>
  <tr>
    <td>jobId</td>
    <td>integer</td>
    <td>ID работы</td>
  </tr>
  <tr>
    <td>completedAt</td>
    <td>integer</td>
    <td>Время завершения работы</td>
  </tr>
  <tr>
    <td>collisionId</td>
    <td>string</td>
    <td>ID статуса уведомления типа "Коллизия"</td>
  </tr>
  <tr>
    <td>createdAt</td>
    <td>integer</td>
    <td>Время создания записи</td>
  </tr>
  <tr>
    <td>notificationId</td>
    <td>string</td>
    <td>ID сгенерированного уведомления</td>
  </tr>
</table>

### M18. Статус работы по отправке уведомления. Тип "Коллизия"


<table>
  <tr>
    <th>Поле</th>
    <th>Тип</th>
    <th>Описание</th>
  </tr>
  <tr>
    <td>id</td>
    <td>string</td>
    <td>ID записи</td>
  </tr>
  <tr>
    <td>status</td>
    <td>integer<br><br>См. модел M1</td>
    <td>Статус выполения</td>
  </tr>
  <tr>
    <td>jobId</td>
    <td>integer</td>
    <td>ID работы</td>
  </tr>
  <tr>
    <td>completedAt</td>
    <td>integer</td>
    <td>Время завершения работы</td>
  </tr>
  <tr>
    <td>collisionId</td>
    <td>string</td>
    <td>ID статуса уведомления типа "Коллизия"</td>
  </tr>
  <tr>
    <td>createdAt</td>
    <td>integer</td>
    <td>Время создания записи</td>
  </tr>
</table>


### MJ1. Данные для генерации уведомления. Тип "Тик"

<table>
  <tr>
    <th>Поле</th>
    <th>Тип</th>
    <th>Описание</th>
  </tr>
  <tr>
    <td>title</td>
    <td>string</td>
    <td>Заголовок</td>
  </tr>
  <tr>
    <td>statusRecordId</td>
    <td>string</td>
    <td>ID статуса работы</td>
  </tr>
  <tr>
    <td>actionId</td>
    <td>string</td>
    <td>ID действия</td>
  </tr>
  <tr>
    <td>tickId</td>
    <td>string</td>
    <td>ID статуса уведомления. Тип "Тик"</td>
  </tr>
</table>

### MJ2. Данные для генерации уведомления. Тип "Коллизия"

<table>
  <tr>
    <th>Поле</th>
    <th>Тип</th>
    <th>Описание</th>
  </tr>
  <tr>
    <td>title</td>
    <td>string</td>
    <td>Заголовок</td>
  </tr>
  <tr>
    <td>statusRecordId</td>
    <td>string</td>
    <td>ID статуса работы</td>
  </tr>
  <tr>
    <td>actionsIds</td>
    <td>string[]</td>
    <td>Список ID действий</td>
  </tr>
  <tr>
    <td>collisionId</td>
    <td>string</td>
    <td>ID статуса уведомления. Тип "Коллизия"</td>
  </tr>
</table>

### MJ3. Данные для отправки уведомления. Тип "Коллизия"

<table>
  <tr>
    <th>Поле</th>
    <th>Тип</th>
    <th>Описание</th>
  </tr>
  <tr>
    <td>title</td>
    <td>string</td>
    <td>Заголовок</td>
  </tr>
  <tr>
    <td>statusRecordId</td>
    <td>string</td>
    <td>ID статуса работы</td>
  </tr>
  <tr>
    <td>collisionId</td>
    <td>string</td>
    <td>ID статуса уведомления. Тип "Коллизия"</td>
  </tr>
</table>

### MJ4. Данные для отправки уведомления. Тип "Тик"

<table>
  <tr>
    <th>Поле</th>
    <th>Тип</th>
    <th>Описание</th>
  </tr>
  <tr>
    <td>title</td>
    <td>string</td>
    <td>Заголовок</td>
  </tr>
  <tr>
    <td>statusRecordId</td>
    <td>string</td>
    <td>ID статуса работы</td>
  </tr>
  <tr>
    <td>tickId</td>
    <td>string</td>
    <td>ID статуса уведомления. Тип "Тик"</td>
  </tr>
</table>

# Расположение

Kue:

 * **Q1**. Очередь **ticks_notification_generation_jobs**. Генерация уведомления типа "Тик" по **MJ1** моделе.
 * **Q2**. Очередь **ticks_notification_sending_jobs**. Отправка уведомления типа "Тик" по **MJ4** моделе.
 * **Q3**. Очередь **task_actions_collision_notification_generation_jobs**. Генерация уведомления типа "Коллизия" по **MJ2** моделе.
 * **Q4**. Очередь **task_actions_collision_notification_sending_jobs**. Отправка уведомления типа "Коллизия" по **MJ3** моделе.

Firestore:

 * **F1**. Коллекция **actions**. Модель **M2**.
 * **F2**. Коллекция **encrypted_account_data**. Модель **M13**.
 * **F3**. Коллекция **notification_intervals**. Модель **M9**.
 * **F4**. Коллекция **notifications_processed_actions**. Модель **M8**.
 * **F6**. Коллекция **organizations**. Модель **M10**.
 * **F7**. Коллекция **task**. Модель **M11**.
 * **F8**. Коллекция **account**. Модель **M12**.
 * **F9**. Коллекция **notifications_task_actions_collision_generation_job_status**. Модель **M17**.
 * **F10**. Коллекция **notifications_task_actions_collision_notifications**. Модель **M14**.
 * **F11**. Коллекция **notifications_task_actions_collision_receiver_sending_status**. Модель **M15**.
 * **F12**. Коллекция **notifications_task_actions_collision_sending_job_status**. Модель **M18**.
 * **F13**. Коллекция **notifications_task_actions_collision_status**. Модель **M16**.
 * **F14**. Коллекция **notifications_ticks_generation_job_status**. Модель **M6**.
 * **F15**. Коллекция **notifications_ticks_notifications**. Модель **M3**.
 * **F16**. Коллекция **notifications_ticks_receiver_sending_status**. Модель **M4**.
 * **F17**. Коллекция **notifications_ticks_sending_job_status**. Модель **M7**.
 * **F18**. Коллекция **notifications_ticks_status**. Модель **M5**.

# Общая конфигурация

 * `REDIS_HOST` - хост redis сервера
 * `REDIS_PORT` - порт redis сервера
 * `REDIS_PASSWORD` - пароль для доступа к redis серверу
 * `FIREBASE_CONFIG` - JSON с даннными авторизации к firebase. [Описание тут](https://firebase.google.com/docs/functions/config-env#automatically_populated_environment_variables).
 * `FIREBASE_DATABASE_URL` - [Описание тут](https://firebase.google.com/docs/reference/admin/node/admin.app.AppOptions#databaseURL)
 * `FIREBASE_FIRESTORE_BUNCH_WRITE_LIMIT` - bunch write лимит для firebase. По умолчанию 500. Нужен для формирования нагрузок на firebase, и обхода предупреждения о превышении лимита.
 * `ENCRYPTED_ACCOUNT_DATA_KEY` - ключ для декодирования зашифрованных данныю пользователей. 24 байта.
 * `GOOGLE_CLIENT_ID` - google app client id
 * `GOOGLE_CLIENT_SECRET` - google app secret
 * `TELEGRAM_HOST_URL` - хост телеграм ноды

# Master

Master выполняет роль контроля и распределения задач. В сети может быть от 1 до нескольких master-в, но работать будет только 1, остальные на случай сбоя лидера. Отбором лидера среди master занимает **cron-cluster**.

Master расширяем плагинами, которые должны реализовать определенный цикл создания и отправки уведомлений.

Цикл состоит из следующийх шагов:

 * onSnapshot - шаг создания слепка состояния. На этом этапе собирается вся необходимая информация из баз данных, kue и от куда угодна для дальнейшей работы.
 * onUpdateGenerationJobStatus - шаг сихронизации процесса генерации уведомлений worker-и между базой данных и kue
 * onUpdateSendingJobStatus - шаг сихронизации процесса отправки уведомлений worker-и между базой данных и kue
 * onCreateSendingStatus - шаг создание статусов в базе данных, для отражения состояния генерации уведомления и рассылки(для примера M16 и M5 модель).
 * onCreateSendingJob - шаг формирования работ по отправке уведомлений для worker-в
 * onNewAction - последовательная обработка каждого нового действия(модель M2), произведенного с момента последней работы master-а
 * onCreateGenerationJob - шаг формирования работ по генерации уведомлений для worker-в
 * onBeforeEnd - шаг завершения работы
 
Плагины должны придерживатся общих принципов:

* Учитывать сбои на каждом шаге и уметь без существенных потерь поддерживать консистентность системы
* Отправлять уведомления в хронологическом порядке
* Придерживатся цикла, описаного выше

АПИ Master

**onSnapshot**(**snapshotData**)

Где **snapshotData**:

```
{
    // Organization - M10 модель. Список организаций.
    organizations: Array<Organization>; 
    
    // Action - M2 модель. Список новых действий.
    newActions: Array<Action>;
    
    // Таймштамп запуска master
    nowDate: number; 
}
```

Если плагин возвращает данные, то они сохраняются для дальнейшого использования.

**onUpdateGenerationJobStatus**(**snapshotData**, **pluginSnapshotData**)

Где **pluginSnapshotData** - любые данные, которые возратил плагин на **onSnapshot** этапе.

**onUpdateSendingJobStatus**(**snapshotData**, **pluginSnapshotData**)

**onCreateSendingStatus**(**snapshotData**, **pluginSnapshotData**, **blocksManager**)

Где **blocksManager**:

```
{
    // Поставить флаг "не отправлять" для действия
    block(actionId: string): void;
    
    // Проверка состояния флага
    isBlock(actionId: string): boolean; 
}
```

**onCreateSendingJob**(**snapshotData**, **pluginSnapshotData**)

**onNewAction**(**snapshotData**, **pluginSnapshotData**, **action**, **commit**)

Где **action** - данные по модели M2

А **commit**

```
{
    // Конструктор одноименного действия при формировании транзации в файрсторе. ref - референс на документ, data - данные.
    Set: { new(ref, data) };
    
    // Конструктор одноименного действия при формировании транзации в файрсторе. ref - референс на документ, data - данные.
    Update: { new(ref, data) };
    
    // Конструктор одноименного действия при формировании транзации в файрсторе. ref - референс на документ.
    Delete: { new(ref) };
    
    // Поставить действия в транзакциию. Статус процессинга действия сохраняется в базу атомарной операцией
    commit(action: Set | Update | Delete);
}
```

**onCreateGenerationJob**(**snapshotData**, **pluginSnapshotData**)

**onBeforeEnd**()

Пока не используется

Настройки:

 * `MASTER_WAKEUP_TIMER` - таймер, по которому Master будет просыпаться
 * `MASTER_ATTEMPTS_BEFORE_CHANGE_LEADER` - число неудачных попыток выполнить работу перед тем как Master-лидер передаст работу другому Master
 * `MASTER_GENERATE_NOTIFICATION_JOB_TTL_MS` - TTL работы по генерации уведоления
 * `MASTER_SENDING_NOTIFICATION_JOB_TTL_MS` - TTL работы по отправке уведомлений

# Ticks notification master plugin

Плагин реализует логику формирования и отправки уведомлений типа "Тик". Для организации момент отправки уведомлений - есть один тик, у тика есть определенный интервал накопления действий, и есть запланированная дата отправка, т.е. триггер тика(отправки). Тик за N времени накапливает бандл действий и сформированных уведомлений для них и отправляет их пачкой в запланированную дату в хронолигическом порядке.

# Task actions collision master plugin

Плагин реализует логику по поиску коллизий в действиях над задачами и отправки уведомлений тип "Коллизия". 

### Понятие коллизии в действиях

Контекстом для коллизий являются задачи. Коллизией действий над задачами являются 2-а и более одинаковых по параметру А и по названию действий, относящихся к задаче, произошедших подряд, исключая другие действия между ними(отличаются по названию).

Параметр А определяется следующим образом:

action - модель данных **M2**
task - модель данных **M11**

 * `task_create` - это действие исключено, коллизий тут не может быть.
 * `task_update_status` - hash(action.organizationId, action.taskId, action.currentValue)
 * `task_change_type`-  hash(action.organizationId, action.taskId, action.currentValue)

Пример для `task_update_status`

Цепочка действий:

1. `task_created` - создалась задача с id 1 в организации с id 1
2. `task_change_type` - hash(1, 1, 'task')
3. `task_update_status` - hash(1, 1, 'active')
4. `task_created` - создалась задача с id 2 в организации с id 1
5. `task_update_status` - hash(1, 1, 'active')

В этой цепочки действия 3 и 5 являются коллизией.

Другой пример:

1. `task_created` - создалась задача с id 1 в организации с id 1
2. `task_chage_type` - hash(1, 1, 'task')
3. `task_update_status` - hash(1, 1, 'active')
4. `task_update_status` - hash(1, 1, 'delayed')
5. `task_update_status` - hash(1, 1, 'active')

Пример схож с прошлым, исключая то, что действие 4 изменяет статус на другое значение так, что 3 и 5 не является коллизией.

### Определение коллизий

Для уведомления возьмем только 2-а действия: текущее и прошлое.

Примерная логика на JS.

```
// Получаем массив организаций из базы данные(коллекция organizations)
const organizations = getOrganizations();
// Получаем массив действий, которые происходили с момента последней работы master
const newActions = getNewActions();
// Список коллизий
const collissions = [];

for (const organization of organizations) {
    const actions = newActions.filter(action => action.organizationId === organization.id);
    if (actions.length !== 0) {
    	for (const action of actions) {
        	let hasCollision = false;
        	switch(action.name) {
            	case 'task_update_status':
              		hasCollision = action.previousValue === action.currentValue;
                	break;
                case 'task_change_type':
              		hasCollision = action.previousValue === action.currentValue;
                	break;
            }
            if (hasCollision) {
            	// Получаем предыдущее действие по параметрам:
                // organizationId === action.organizationId
                // taskId === action.taskId
                // name === action.name
                // Сортировка по createdAt DESC
				// Исключаются действия, начиная от action(включая его)
                // Лимит результатов === 1
            	const previousAction = getPreviousAction(action, fromDate);
                // Исключено отсутствие предыдущего действия, но гипотетически...
                if (previousAction) {
                	collisions.push([previousAction, action]);
                }
            }
        }
    }
}
```

Бизнес логика гласить, что коллизии должны сразу же определяется и уведемление должно идти вне очереди. Плагин использует blockActions функционал на шаге onCreateSendingStatus, чтобы не дать отправить другие уведомления(другого типа), до тех пор, пока не отправить уведомления по коллизиям. К примеру, оповещение о коллизии должно придти раньше, чем отправится тик.

# Worker

Воркеры реализуют всю логику по генерации уведомлений и отправке. **Kue** автоматически распределяет работы по Worker, можно в сети запускать от 1 до нескольких инстансов.

На данный момент под каждый тип уведомлений есть свои воркеры. Все воркеры можно запускать как единый процесс, либо разбрасывать их по отдельности. 

Настройки:

 * `JOBS_COUNT` - кол-во работы, которые может взять воркер. Нужно обдумать этот параметр, т.к. Worker можно форкать по CPU, с помощью node js cluster модуля и там уже каждый форк может брать по `JOBS_COUNT` параллельно или как-то так.

# Логирование и мониторинг

Сбор и мониторинг логов - задача отдельного сервиса. 

# Деплой

Настройки докер контейнера:

**ENTRYPOINT_NAME** - может быть **master** или **worker**. Указываем какой инстанс запустить в контейнере.
